using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using radio_backend.Models.CityInformation;
using radio_backend.Models.General;
using radio_backend.Models.Radio;
using radio_backend.Models.Request;
using radio_backend.Models.WeatherForecast;

namespace radio_backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddSingleton<RequestFactory>();
            services.AddSingleton<DataCache>();

            // Add retry policy for HTTPClient
            // see: https://docs.microsoft.com/en-us/dotnet/architecture/microservices/implement-resilient-applications/implement-http-call-retries-exponential-backoff-polly
            services.AddHttpClient<IRequestHelper, RequestHelper>();
            // Adding the named HttpClient to our class already will register said class
            // The following call would override this behaviour (see: //https://github.com/dotnet/extensions/issues/1079)
            //services.AddTransient<IRequestHelper, RequestHelper>();

            services.AddSingleton<IRadioStationFetcher, RadioStationFetcher>();
            services.AddSingleton<IWeatherForecastFetcher, WeatherForecastFetcher>();
            services.AddSingleton<ICityInformationFetcher, CityInformationFetcher>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            if (env.IsDevelopment())
            {
                app.UseCors(options => options.WithOrigins("https://localhost:3000",
                    "http://localhost:3000").AllowAnyMethod());
            }
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
