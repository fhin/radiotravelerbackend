﻿using Newtonsoft.Json;
using System;

namespace radio_backend.Models.WeatherForecast
{
    public enum UnitType
    {
        IMPERIAL, METRIC, STANDARD // Kelvin
    }

    public class WeatherForecast
    {
        [JsonProperty(PropertyName = "unitType")]
        public UnitType Unit { get; set; }

        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }
        [JsonProperty(PropertyName = "minTemp")]
        public int MinTemperature { get; set; }

        [JsonProperty(PropertyName = "maxTemp")]
        public int MaxTemperature { get; set; }

        [JsonProperty(PropertyName = "rainfall")]
        public int Rainfall { get; set; }

        [JsonProperty(PropertyName = "humidity")]
        public int Humidity { get; set; }

        [JsonProperty(PropertyName = "generalWeather")]
        public string GeneralWeather { get; set; }

        [JsonProperty(PropertyName = "weatherDescription")]
        public string WeatherDescription { get; set; }

        [JsonProperty(PropertyName = "icon")]
        public byte[] WeatherIcon { get; set; }

        public WeatherForecast()
        {
            Unit = UnitType.METRIC;
            WeatherIcon = new byte[0];
        }
    }
}
