﻿using Newtonsoft.Json.Linq;
using radio_backend.Models.General;
using radio_backend.Models.Request;
using radio_backend_models.GeneralConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace radio_backend.Models.WeatherForecast
{
    public class WeatherForecastFetcher : IWeatherForecastFetcher
    {
        private readonly IRequestHelper _reqHelper;
        private readonly RequestFactory _reqFactory;
        private DataCache _dataCache;

        public WeatherForecastFetcher(IRequestHelper requestHelper, RequestFactory requestFactory, DataCache dataCache)
        {
            _reqHelper = requestHelper;
            _reqFactory = requestFactory;
            _dataCache = dataCache;
        }

        public async Task<IEnumerable<WeatherForecast>> GetForecasts(double latitude, double longitude, bool hourly, UnitType unitType)
        {
            if (_reqHelper == null || _reqFactory == null) throw new InvalidOperationException("");
            IEnumerable<WeatherForecast> cachedForecasts = _dataCache.LookupForecasts(latitude, longitude, !hourly);
            if (cachedForecasts != null && cachedForecasts.Any())
            {
                return cachedForecasts.Take(GeneralConstants.MAX_WEATHER_FORECAST_PER_FETCH);
            }

            Request.Request weatherRequest = _reqFactory.GetRequest(
                hourly ? RequestType.HOURLY_WEATHER_FORECAST : RequestType.DAILY_WEATHER_FORECAST);
            weatherRequest.AddActualParameter(RequestConstants.WREQ_LATITUDE_KEY, latitude);
            weatherRequest.AddActualParameter(RequestConstants.WREQ_LONGITUDE_KEY, longitude);

            string unitKey = RequestConstants.STANDARD_UNIT_KEY;
            if (unitType == UnitType.METRIC)
            {
                unitKey = RequestConstants.METRIC_UNIT_KEY;
            }
            else if (unitType == UnitType.IMPERIAL)
            {
                unitKey = RequestConstants.IMPERIAL_UNIT_KEY;
            }
            weatherRequest.AddActualParameter(RequestConstants.WREQ_UNIT_KEY, unitKey);

            HttpResponseMessage httpResponse = await _reqHelper.SendRequest(weatherRequest, true);
            if (!httpResponse.IsSuccessStatusCode) throw new ArgumentException("");

            IEnumerable<WeatherForecast> fetchedForecasts = await ParseJson(await httpResponse.Content.ReadAsStringAsync(), !hourly);

            try
            {
                if (!_dataCache.CacheForecasts(latitude, longitude, !hourly, fetchedForecasts))
                {
                    // TODO: Maybe log that caching did not work
                }
            }
            catch (Exception)
            {
                // TODO: Logging
            }
            return fetchedForecasts;
        }

        // Json response structure can be found @ https://openweathermap.org/api/one-call-api
        private async Task<IEnumerable<WeatherForecast>> ParseJson(string jsonResponse, bool dailyResponse)
        {
            JObject responseObject = JObject.Parse(jsonResponse);
            var weatherJsonObjects = responseObject[dailyResponse ? RequestConstants.DAILY_JSON_KEY :
               RequestConstants.HOURLY_JSON_KEY].Children<JObject>();

            var weatherParsingTasks = weatherJsonObjects
                .Take(GeneralConstants.MAX_WEATHER_FORECAST_PER_FETCH)
                .Select(async (weatherJsonObject, idx) =>
            {
                try
                {
                    WeatherForecast forecast = new WeatherForecast
                    {
                        Date = DateTime.UtcNow
                    };

                    if (dailyResponse)
                    {
                        forecast.Date = forecast.Date.AddDays(idx);
                        forecast.MaxTemperature = Convert.ToInt32(weatherJsonObject[RequestConstants.TEMP_JSON_KEY][RequestConstants.TEMP_MAX_JSON_KEY]);
                        forecast.MinTemperature = Convert.ToInt32(weatherJsonObject[RequestConstants.TEMP_JSON_KEY][RequestConstants.TEMP_MIN_JSON_KEY]);
                        forecast.Rainfall = Convert.ToInt32(weatherJsonObject[RequestConstants.RAIN_JSON_KEY]);
                    }
                    else
                    {
                        forecast.Date = forecast.Date.AddHours(idx);
                        forecast.MaxTemperature = Convert.ToInt32(weatherJsonObject[RequestConstants.TEMP_JSON_KEY]);
                        forecast.MinTemperature = forecast.MaxTemperature;
                        // Rainfall information is not always available for hourly forecasts
                        try
                        {
                            forecast.Rainfall = Convert.ToInt32(weatherJsonObject[RequestConstants.RAIN_JSON_KEY][RequestConstants.HOURLY_RAIN_KEY]);
                        }
                        catch (Exception)
                        {
                            forecast.Rainfall = 0;
                        }
                    }
                    forecast.Humidity = Convert.ToInt32(weatherJsonObject[RequestConstants.HUMID_JSON_KEY]);
                    JToken weatherProperty = ((JArray) weatherJsonObject[RequestConstants.WEATHER_JSON_KEY])[0];
                    forecast.GeneralWeather = weatherProperty[RequestConstants.WEATHER_MAIN_JSON_KEY].ToString();
                    forecast.WeatherDescription = weatherProperty[RequestConstants.WEATHER_DESCR_JSON_KEY].ToString();
                    // TODO: Query weather icon, maybe cache icons
                    string weatherIconKey = weatherProperty[RequestConstants.WEATHER_ICON_JSON_KEY].ToString();
                    // TODO: Fallback method if fetching of icon fails
                    forecast.WeatherIcon = await GetWeatherIcon(weatherIconKey);
                    return forecast;
                }
                catch (Exception)
                {
                    // TODO: Handle exception
                    return null;
                }
            }).Where(wForecast => wForecast != null);
            return await Task.WhenAll(weatherParsingTasks);
        }

        private async Task<byte[]> GetWeatherIcon(string weatherIdKey)
        {
            if (string.IsNullOrWhiteSpace(weatherIdKey)) throw new ArgumentException("");
            byte[] weatherIcon = _dataCache.LookupWeatherIcon(weatherIdKey);
            if (weatherIcon.Length != 0) return weatherIcon;

            // Icon cache miss
            Request.Request wIconRequest = _reqFactory.GetRequest(RequestType.WEATHER_ICON);
            wIconRequest.AddBaseUrlParameter(weatherIdKey);
            wIconRequest.AddBaseUrlParameter(RequestConstants.WEATHER_ICON_SMALL);
            HttpResponseMessage httpResponse = await _reqHelper.SendRequest(wIconRequest, true);
            if (!httpResponse.IsSuccessStatusCode) throw new ArgumentException("");
            // Decode weather icon response
            weatherIcon = await httpResponse.Content.ReadAsByteArrayAsync();
            // Cache weather icon
            _dataCache.CacheWeatherIcon(weatherIdKey, weatherIcon);
            return weatherIcon;
        }
    }
}
