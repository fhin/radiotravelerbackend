﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace radio_backend.Models.WeatherForecast
{
    public interface IWeatherForecastFetcher
    {
        public Task<IEnumerable<WeatherForecast>> GetForecasts(double latitude, double longitude, bool hourly, UnitType unitType);
    }
}
