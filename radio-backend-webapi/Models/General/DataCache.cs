﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using radio_backend.Controllers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.CodeAnalysis;
using radio_backend_models.GeneralConstants;
using radio_backend_models.Radio;
using radio_backend.Models.CityInformation;

namespace radio_backend.Models.General
{
    public class DataCache
    {
        private IDictionary<string, byte[]> _weatherIconDict;
        private readonly RadioStation[] _availableRadioStations;
        private IDictionary<(double, double), byte> _fetchedLocations;
        private readonly string _resourcesName = "Resources\\places.txt";
        private IDictionary<(double, double, bool), IEnumerable<WeatherForecast.WeatherForecast>> _forecastCache;
        private IDictionary<(string, string), (int, int, IEnumerable<WikiSection>)> _wikiSectionDict;
        private IDictionary<(string, string), CityInformation.CityInformation> _cityInfoDict;

        public DataCache()
        {
            _weatherIconDict = new ConcurrentDictionary<string, byte[]>();
            try
            {
                _availableRadioStations = LoadRadioStations().ToArray();
            }
            catch (Exception)
            {
                // TODO: Exception handling
                _availableRadioStations = new RadioStation[0];
            }
            _fetchedLocations = new ConcurrentDictionary<(double, double), byte>();
            _forecastCache = new ConcurrentDictionary<(double, double, bool), IEnumerable<WeatherForecast.WeatherForecast>>();
            _wikiSectionDict = new ConcurrentDictionary<(string, string), (int, int, IEnumerable<WikiSection>)>();
            _cityInfoDict = new ConcurrentDictionary<(string, string), CityInformation.CityInformation>();
        }

        public int GetAvailableStationCnt()
        {
            return _availableRadioStations.Length;
        }

        public IEnumerable<RadioStation> LoadRadioStations(IEnumerable<int> radioStationIdxs)
        {
            if (radioStationIdxs == null) return new RadioStation[0];
            int[] idxArr = radioStationIdxs.ToArray();
            return LookupRadioStations(radioStationIdxs).Select((isValid, index) =>
            {
                return !isValid ? null : _availableRadioStations[idxArr[index]];
            });
        }

        public IEnumerable<bool> LookupRadioStations(IEnumerable<int> radioStationIdxs)
        {
            if (radioStationIdxs == null) return new bool[0];
            int nonCachedEntries = 0;
            return radioStationIdxs.Select(radioStationIdx =>
            {
                if (radioStationIdx < 0 || radioStationIdx > _availableRadioStations.Length) return false;
                RadioStation _cachedRadioStation = _availableRadioStations[radioStationIdx];
                (double, double) cachedRadioLocation = (_cachedRadioStation.Latitude, _cachedRadioStation.Longitude);
                if (_fetchedLocations.ContainsKey(cachedRadioLocation))
                {
                    return true;
                }
                if (_fetchedLocations.Count + (nonCachedEntries+1) < GeneralConstants.MAX_REQUEST_LIMIT)
                {
                    nonCachedEntries++;
                    return true;
                }
                return false;
            });
        }

        public IEnumerable<WeatherForecast.WeatherForecast> LookupForecasts(double latitude, double longitude, bool isDailyForecast)
        {
            List<WeatherForecast.WeatherForecast> cachedResponse = new List<WeatherForecast.WeatherForecast>();

            var locationKey = (longitude, latitude, isDailyForecast);
            if (_forecastCache == null || !_forecastCache.ContainsKey(locationKey)) return cachedResponse;
            try
            {
                return _forecastCache[locationKey];
            }
            catch (KeyNotFoundException)
            {
                return cachedResponse;
            }
        }

        public bool CacheForecasts(double latitude, double longitude, bool isDailyForecast,
            IEnumerable<WeatherForecast.WeatherForecast> fetchedForecasts)
        {
            if (_forecastCache == null) return false;
            if (fetchedForecasts == null || !fetchedForecasts.Any()) return false;

            var locationKey = (longitude, latitude, isDailyForecast);
            if (fetchedForecasts.Any(forecast => forecast == null)) throw new ArgumentException("");

            if (_forecastCache.ContainsKey(locationKey))
            {
                _forecastCache[locationKey] = fetchedForecasts.Take(GeneralConstants.MAX_WEATHER_FORECAST_PER_FETCH);
            }
            else
            {
                _forecastCache.Add(locationKey, fetchedForecasts.Take(GeneralConstants.MAX_WEATHER_FORECAST_PER_FETCH));
            }
            return true;
        }

        public bool CacheRadioStation(int radioStationIdx)
        {
            if (radioStationIdx < 0 || radioStationIdx > _availableRadioStations.Length) throw new ArgumentException();
            RadioStation _cachedRadioStation = _availableRadioStations[radioStationIdx];
            (double, double) radioStationCoords = (_cachedRadioStation.Latitude, _cachedRadioStation.Longitude);
            if (_fetchedLocations.ContainsKey(radioStationCoords)) return false;
            _fetchedLocations.Add(radioStationCoords, 0);
            return true;
        }

        public byte[] LookupWeatherIcon(string weatherIconName)
        {
            byte[] emptyIcon = new byte[0];
            if (string.IsNullOrWhiteSpace(weatherIconName)) return new byte[0];
            if (_weatherIconDict.ContainsKey(weatherIconName)) return _weatherIconDict[weatherIconName];
            return emptyIcon;
        }

        public void CacheWeatherIcon(string weatherIconName, byte[] iconArr)
        {
            if (string.IsNullOrWhiteSpace(weatherIconName) || _weatherIconDict.ContainsKey(weatherIconName)) return;
            _weatherIconDict.Add(weatherIconName, iconArr);
        }

        private IEnumerable<RadioStation> LoadRadioStations()
        {
            string streamContent = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(_resourcesName))
                {
                    throw new FileNotFoundException(ErrConstants.ERR_LOADING_STATIONS);
                }
                using (var streamReader = new StreamReader(_resourcesName, System.Text.Encoding.UTF8))
                {
                    streamContent = streamReader.ReadToEnd();
                }
            }
            catch (FileNotFoundException) { }
            catch (DirectoryNotFoundException) { }
            catch (System.NotSupportedException) { }

            try
            {
                var radioEntries = JsonConvert.DeserializeObject(streamContent) as JArray; 
                return radioEntries.Select(radioEntry =>
                {
                    try
                    {
                        return new RadioStation
                        {
                            Id = radioEntry.Value<string>(GeneralConstants.RADIO_ID_KEY),
                            Name = radioEntry.Value<string>(GeneralConstants.RADIO_STATIONNAME_KEY),
                            CityName = radioEntry.Value<string>(GeneralConstants.RADIO_CITYNAME_KEY),
                            CountryName = radioEntry.Value<string>(GeneralConstants.RADIO_COUNTRYNAME_KEY),
                            AudioStreamURI = radioEntry.Value<string>(GeneralConstants.RADIO_AUDIOSTREAM_KEY),
                            Latitude = radioEntry.Value<double>(GeneralConstants.RADIO_STATION_LATITUDE_KEY),
                            Longitude = radioEntry.Value<double>(GeneralConstants.RADIO_STATION_LONGITUDE_KEY)
                        };
                    }
                    catch (System.Exception)
                    {
                        return null;
                    }
                }).Where(radioStation => radioStation != null).ToArray();
            }
            catch (System.Exception e)
            {
                int x = 0;
            }
            return new RadioStation[0];
        }
   
        public (int, int, IEnumerable<WikiSection>) LookupTravelWikiStructureCache(string cityName, string countryName)
        {
            if (string.IsNullOrWhiteSpace(cityName) || string.IsNullOrWhiteSpace(countryName)) return (-1, -1, new WikiSection[0]);
            try
            {
                (string, string) key = (cityName, countryName);
                if (_wikiSectionDict.ContainsKey(key)) return _wikiSectionDict[key];
                else return (-1, -1, new WikiSection[0]);
            }
            catch (Exception)
            {
                throw new ArgumentException("Error while looking up wiki structure cache for " + cityName + "|" + countryName);
            }
        }

        public void CacheTravelWikiStructure(string cityName, string countryName, int pageId, int travelWikiId, IEnumerable<WikiSection> structure)
        {
            if (string.IsNullOrWhiteSpace(cityName) || string.IsNullOrWhiteSpace(countryName) || structure == null|| pageId < 0 || travelWikiId < 0) return;
            try
            {
                (string, string) key = (cityName, countryName);
                (int, int, IEnumerable<WikiSection>) value = (pageId, travelWikiId, structure);
                if (_wikiSectionDict.ContainsKey(key))
                {
                    _wikiSectionDict[key] = value;
                }
                else
                {
                    _wikiSectionDict.Add(key, value);
                }
            }
            catch (Exception)
            {
                throw new ArgumentException("Error while looking up wiki structure cache for " + cityName + "|" + countryName);
            }
        }
    
        public CityInformation.CityInformation LookupCityInformation(string cityName, string countryName)
        {
            if (string.IsNullOrWhiteSpace(cityName) || string.IsNullOrWhiteSpace(countryName)) return null;
            try
            {
                (string, string) key = (cityName, countryName);
                return _cityInfoDict[key];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void CacheCityInformation(string cityName, string countryName, CityInformation.CityInformation cityInfo)
        {
            if (string.IsNullOrWhiteSpace(cityName) || string.IsNullOrWhiteSpace(countryName) || cityInfo == null) return;
            try
            {
                (string, string) key = (cityName, countryName);
                if (_cityInfoDict.ContainsKey(key))
                {
                    _cityInfoDict[key] = cityInfo;
                }
                else
                {
                    _cityInfoDict.Add(key, cityInfo);
                }
            }
            catch (Exception)
            {
                throw new ArgumentException("");
            }
        }

    }
}
