﻿namespace radio_backend.Models.CityInformation
{
    public class WikiSection
    {
        public string SectionName { get; set; }
        public uint SectionIndex { get; set; }
        public string SectionNumber { get; set; }
    }
}
