﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace radio_backend.Models.CityInformation
{
    public class CityInformation
    {
        [JsonProperty("cityName")]
        public string CityName { get; set; }
        [JsonProperty("countryName")]
        public string CountryName { get; set; }
        [JsonProperty("summary")]
        public string Summary { get; set; }
        [JsonProperty("sections")]
        public IEnumerable<(string, string)> SectionsContent { get; set; }
    }
}
