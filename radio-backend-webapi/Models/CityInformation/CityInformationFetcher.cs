﻿using Newtonsoft.Json.Linq;
using radio_backend.Models.General;
using radio_backend.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace radio_backend.Models.CityInformation
{
    public class CityInformationFetcher : ICityInformationFetcher
    {
        private readonly string CLIMATE_KEY = "Climate";
        private readonly string GET_IN_KEY = "Get in";
        private readonly string GET_AROUND_KEY = "Get around";
        private readonly string DO_CATEGORY_KEY = "Do";
        private readonly string SEE_CATEGORY_KEY = "See";
        private readonly string EAT_CATEGORY_KEY = "Eat";
        private readonly string DRINK_CATEGORY_KEY = "Drink";
        private readonly IDictionary<CityInformationCategory, string> _cityInfoPropDict;
        private DataCache _dataCache;
        private IRequestHelper _reqHelper;
        private RequestFactory _reqFactory;
        private IEnumerable<CityInformationCategory> _cityInfoCatOrder;

        public CityInformationFetcher(DataCache dataCache, IRequestHelper requestHelper, RequestFactory requestFactory)
        {
            _cityInfoPropDict = FillCityInfoDict();
            _dataCache = dataCache;
            _reqHelper = requestHelper;
            _reqFactory = requestFactory;
            _cityInfoCatOrder = new CityInformationCategory[] { CityInformationCategory.CLIMATE, CityInformationCategory.GET_IN, 
                CityInformationCategory.GET_AROUND, CityInformationCategory.SEE, CityInformationCategory.DO,
                CityInformationCategory.EAT, CityInformationCategory.DRINK};
        }

        private IDictionary<CityInformationCategory, string> FillCityInfoDict()
        {
            IDictionary<CityInformationCategory, string> dict = new Dictionary<CityInformationCategory, string>(7)
            {
                { CityInformationCategory.CLIMATE, CLIMATE_KEY },
                { CityInformationCategory.GET_IN, GET_IN_KEY },
                { CityInformationCategory.GET_AROUND, GET_AROUND_KEY },
                { CityInformationCategory.DO, DO_CATEGORY_KEY },
                { CityInformationCategory.SEE, SEE_CATEGORY_KEY },
                { CityInformationCategory.EAT, EAT_CATEGORY_KEY },
                { CityInformationCategory.DRINK, DRINK_CATEGORY_KEY }
            };
            return dict;
        }

        public Task<CityInformation> FetchCityInformation(string cityName, string countryOfOrigin, CityInformationCategory cityInformationCategory)
        {
            throw new NotImplementedException();
        }

        public async Task<CityInformation> GetAllCityInformaion(string cityName, string countryOfOrigin)
        {
            if (string.IsNullOrWhiteSpace(cityName) || string.IsNullOrWhiteSpace(countryOfOrigin)) throw new ArgumentException("");
            try
            {
                if (_dataCache.LookupCityInformation(cityName, countryOfOrigin) != null)
                {
                    return _dataCache.LookupCityInformation(cityName, countryOfOrigin);
                }
                (int wikiPageId, int travelWikiPageId, IEnumerable<WikiSection> travelWikiStructure) =
                        _dataCache.LookupTravelWikiStructureCache(cityName, countryOfOrigin);
                if (wikiPageId == -1 || travelWikiPageId == -1)
                {
                    var request = _reqFactory.GetRequest(RequestType.TRAVEL_WIKI_ALL_SECTIONS);
                    request.AddActualParameter(RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, cityName);
                    var response = await _reqHelper.SendRequest(request, true);
                    if (!response.IsSuccessStatusCode) throw new ArgumentException("");

                    var responseJsonObj = JObject.Parse(await response.Content.ReadAsStringAsync());
                    travelWikiPageId = responseJsonObj[RequestConstants.WIKI_API_PARSE_HEAD_KEY].Value<int>(RequestConstants.WIKI_PAGEID_KEY);
                    var jsonSectionArr = responseJsonObj[RequestConstants.WIKI_API_PARSE_HEAD_KEY][RequestConstants.WIKI_API_JSON_SECTIONS_KEY]
                        .Children();
                    travelWikiStructure = jsonSectionArr.Select(jsonSection =>
                    {
                        try
                        {
                            return new WikiSection()
                            {
                                SectionName = jsonSection.Value<string>(RequestConstants.WIKI_API_JSON_SECTION_NAME_KEY),
                                SectionIndex = jsonSection.Value<uint>(RequestConstants.WIKI_API_JSON_SECTION_INDEX_KEY),
                                SectionNumber = jsonSection.Value<string>(RequestConstants.WIKI_API_JSON_SECTION_NUMBER_KEY)
                            };
                        }
                        catch (Exception)
                        {
                            // TODO: Exception handling
                            return null;
                        }
                    }).Where(parsedSection => parsedSection != null && !string.IsNullOrWhiteSpace(parsedSection.SectionName)
                        && parsedSection.SectionIndex >= 0 && !string.IsNullOrWhiteSpace(parsedSection.SectionNumber));
                }
                var travelWikiStructDict = travelWikiStructure
                    .Where(wikiSection => wikiSection != null && !string.IsNullOrWhiteSpace(wikiSection.SectionNumber))
                    .GroupBy(wikiSection => wikiSection.SectionName)
                    .Select(group => group.First())
                    .ToDictionary(wikiSection => wikiSection.SectionName);

                var sectionContentTasks = _cityInfoCatOrder.Select(async cityInformationCategory =>
                {
                    var keyForCityInfoCat = _cityInfoPropDict[cityInformationCategory];
                    string content = string.Empty;
                    try
                    {
                        var sectionIndex = travelWikiStructDict[keyForCityInfoCat].SectionIndex;
                        var cityInfoCatRequest = _reqFactory.GetRequest(RequestType.TRAVEL_WIKI_ONE_SECTION_CONTENT);
                        cityInfoCatRequest.AddActualParameter(RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, cityName);
                        cityInfoCatRequest.AddActualParameter(RequestConstants.WIKI_SECTION_NUMBER_PREFIX, sectionIndex);

                        var response = await _reqHelper.SendRequest(cityInfoCatRequest, true);
                        if (!response.IsSuccessStatusCode) return (keyForCityInfoCat, null);

                        var responseJsonObj = JObject.Parse(await response.Content.ReadAsStringAsync());
                        var contentJsonObj = (JProperty) responseJsonObj[RequestConstants.WIKI_API_PARSE_HEAD_KEY]
                            [RequestConstants.WIKI_API_PARSE_SECTION_CONTENT_KEY].First();
                        content = contentJsonObj.Value.ToString();
                    }
                    catch (Exception)
                    {
                        // TODO: Error handling, if needed
                    }
                    return (keyForCityInfoCat, content);
                });
                var cityGeneralInfoTask = Task.Run<(int, string)>(async () =>
                {
                    var generalCityInfoRequest = _reqFactory.GetRequest(RequestType.WIKI_SUMMARY);
                    generalCityInfoRequest.AddActualParameter(RequestConstants.WIKI_QUERY_PAGE_TITLE_PREFIX, cityName);
                    var response = await _reqHelper.SendRequest(generalCityInfoRequest, true);
                    if (!response.IsSuccessStatusCode) return (-1, string.Empty);

                    var responseJsonObj = JObject.Parse(await response.Content.ReadAsStringAsync());
                    var contentJsonObj = (JProperty) responseJsonObj[RequestConstants.WIKI_API_QUERY_HEAD_KEY]
                        [RequestConstants.WIKI_API_QUERY_CONTENT_KEY].First();

                    int wikiPageId = Convert.ToInt32(contentJsonObj.Name);
                    var content = contentJsonObj.Value[RequestConstants.WIKI_API_QUERY_SUMMARY_KEY].ToString();
                    return (wikiPageId, content);
                });
                (int fetchedWikiPageId, string cityGeneralInfo) = await cityGeneralInfoTask;
                if (string.IsNullOrWhiteSpace(cityGeneralInfo))
                {
                    cityGeneralInfo = string.Empty;
                }
                var categoriesContent = await Task.WhenAll(sectionContentTasks);
                categoriesContent = categoriesContent.Where(kvPair => !string.IsNullOrWhiteSpace(kvPair.keyForCityInfoCat) && kvPair.content != null)
                    .ToArray();

                try
                {
                    _dataCache.CacheTravelWikiStructure(cityName, countryOfOrigin, wikiPageId, travelWikiPageId, travelWikiStructure);
                }
                catch (Exception)
                {
                    throw new ArgumentException("");
                }

                return new CityInformation()
                {
                    CityName = cityName,
                    CountryName = countryOfOrigin,
                    Summary = cityGeneralInfo,
                    SectionsContent = categoriesContent
                };
            }
            catch (Exception e)
            {
                throw new ArgumentException("");
            }
        }
    }
}
