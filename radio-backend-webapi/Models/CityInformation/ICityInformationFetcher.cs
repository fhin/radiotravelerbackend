﻿using System.Threading.Tasks;

namespace radio_backend.Models.CityInformation
{
    public enum CityInformationCategory
    {
        CLIMATE,
        GET_IN,
        GET_AROUND,
        SEE,
        DO,
        EAT,
        DRINK
    }

    public interface ICityInformationFetcher
    {
        public Task<CityInformation> FetchCityInformation(string cityName, string countryOfOrigin, CityInformationCategory cityInformationCategory);
        public Task<CityInformation> GetAllCityInformaion(string cityName, string countryOfOrigin);
    }
}
