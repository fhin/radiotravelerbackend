﻿using radio_backend.Controllers;
using radio_backend.Models.General;
using radio_backend_models.GeneralConstants;
using radio_backend_models.Radio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace radio_backend.Models.Radio
{
    public class RadioStationFetcher : IRadioStationFetcher
    {
        private readonly Random _randomGenerator;
        private readonly int _maxStationIndex;
        private readonly DataCache _cache;

        public RadioStationFetcher(DataCache cache)
        {
            _randomGenerator = new Random();
            _cache = cache;
            _maxStationIndex = _cache.GetAvailableStationCnt();
        }

        public async Task<IEnumerable<RadioStation>> FetchRandomStations()
        {
            var randomIndizes = await GenerateRandomIndices();
            return _cache.LoadRadioStations(randomIndizes).ToArray();
        }
        
        private Task<int[]> GenerateRandomIndices()
        {
            if (_maxStationIndex == 0) return Task.FromResult(new int[0]);
            try
            {
                return Task.Run(() =>
                {
                    ISet<int> fetchedIndizes = new HashSet<int>(GeneralConstants.MAX_STATIONS_PER_FETCH);
                    int[] randomIndizes = new int[GeneralConstants.MAX_STATIONS_PER_FETCH];
                    int[] generatedIndices = new int[GeneralConstants.MAX_STATIONS_PER_FETCH];
                    int[] idxToRefetch = new int[GeneralConstants.MAX_STATIONS_PER_FETCH];
                    bool[] idxValidFlags = new bool[GeneralConstants.MAX_STATIONS_PER_FETCH];

                    int randomArrIdx = 0;
                    int refetchIdxCnt = GeneralConstants.MAX_STATIONS_PER_FETCH;
                    int retryIdx = 0;
                    int currGeneratedIdxToCheck;
                    while (randomArrIdx < GeneralConstants.MAX_STATIONS_PER_FETCH && 
                        refetchIdxCnt > 0 && retryIdx < GeneralConstants.MAX_REQUEST_TRIES)
                    {
                        for (int i = 0; i < refetchIdxCnt; i++)
                        {
                            generatedIndices[i] = _randomGenerator.Next(0, _maxStationIndex);
                        }
                        refetchIdxCnt = 0;
                        idxValidFlags = _cache.LookupRadioStations(generatedIndices).ToArray();
                        for (int flagIdx = 0; flagIdx < idxValidFlags.Length; flagIdx++)
                        {
                            currGeneratedIdxToCheck = generatedIndices[flagIdx];
                            if (idxValidFlags[flagIdx] && !fetchedIndizes.Contains(currGeneratedIdxToCheck))
                            {
                                randomIndizes[randomArrIdx++] = generatedIndices[flagIdx];
                                fetchedIndizes.Add(currGeneratedIdxToCheck);
                            }
                            else
                            {
                                refetchIdxCnt++;                               
                            }
                        }
                    }
                    if (refetchIdxCnt > 0)
                    {
                        throw new Exception(ErrConstants.ERR_IDX_MAX_RETRIES_REACHED);
                    }
                    return randomIndizes;
                });
            }
            catch (Exception)
            {
                throw new Exception(ErrConstants.ERR_IDX_GENERATION);
            }
        }
    }
}
