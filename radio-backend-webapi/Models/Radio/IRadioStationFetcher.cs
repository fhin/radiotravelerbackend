﻿using radio_backend_models.Radio;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace radio_backend.Models.Radio
{
    public interface IRadioStationFetcher
    {
        public Task<IEnumerable<RadioStation>> FetchRandomStations();
    }
}
