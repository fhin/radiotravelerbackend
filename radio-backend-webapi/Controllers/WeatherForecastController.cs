﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using radio_backend.Models.WeatherForecast;
using radio_backend_models.GeneralConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace radio_backend.Controllers
{
    [Route("api/weatherforecast")]
    [ApiController]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IWeatherForecastFetcher _wForecastFetcher;
        private readonly JsonSerializerSettings _jsonSerializerSettings;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IWeatherForecastFetcher wForecasteFetcher)
        {
            _logger = logger;
            _wForecastFetcher = wForecasteFetcher;
            _jsonSerializerSettings = new JsonSerializerSettings()
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                NullValueHandling = NullValueHandling.Include,
                Formatting = Formatting.Indented
            };
        }

        [HttpGet("daily/{latitude:double:required}/{longitude:double:required}/{useMetric=true:bool}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<WeatherForecast[]>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<WeatherForecast>>> GetDailyForecast(double latitude, double longitude, bool useMetric)
        {
            try
            {
                var resultTask = _wForecastFetcher.GetForecasts(latitude, longitude, false, useMetric ? UnitType.METRIC : UnitType.IMPERIAL);
                var fetchedForecasts = await resultTask;
                return Ok(JsonConvert.SerializeObject(fetchedForecasts
                    .Take(GeneralConstants.MAX_WEATHER_FORECAST_PER_FETCH)
                    .Where(forecast => forecast != null)
                    .AsEnumerable(), _jsonSerializerSettings));
            }
            catch (Exception e)
            {
                _logger.LogWarning("There was an error fetching the daily weatherforecasts ! \n Reason: " + e.Message + "\n Stacktrace: " + e.StackTrace);
                return NotFound("Something went wrong !");
            }
        }

        [HttpGet("hourly/{latitude:double:required}/{longitude:double:required}/{useMetric=true:bool}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<WeatherForecast[]>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<WeatherForecast>>> GetHourlyForecast(double latitude, double longitude, bool useMetric)
        {
            try
            {
                var resultTask = _wForecastFetcher.GetForecasts(latitude, longitude, true, useMetric ? UnitType.METRIC : UnitType.IMPERIAL);
                var fetchedForecasts = await resultTask;
                return Ok(JsonConvert.SerializeObject(fetchedForecasts
                    .Take(GeneralConstants.MAX_WEATHER_FORECAST_PER_FETCH)
                    .Where(forecast => forecast != null)
                    .AsEnumerable(), _jsonSerializerSettings));
            }
            catch (Exception e)
            {
                _logger.LogWarning("There was an error fetching the daily weatherforecasts ! \n Reason: " + e.Message + "\n Stacktrace: " + e.StackTrace);
                return NotFound("Something went wrong !");
            }
        }
    }
}