﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using radio_backend.Models.CityInformation;
using System;
using System.Threading.Tasks;

namespace radio_backend.Controllers
{
    [Route("api/cityInfo")]
    [ApiController]
    public class CityInformationController : ControllerBase
    {
        private readonly ILogger<CityInformationController> _logger;
        private readonly ICityInformationFetcher _cityInfoFetcher;
        private readonly JsonSerializerSettings _jsonSerializerSettings;

        public CityInformationController(ILogger<CityInformationController> logger, ICityInformationFetcher cityInformationFetcher)
        {
            _logger = logger;
            _cityInfoFetcher = cityInformationFetcher; 
            _jsonSerializerSettings = new JsonSerializerSettings()
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                NullValueHandling = NullValueHandling.Include,
                Formatting = Formatting.Indented
            };
        }

        [HttpGet("location/{cityName:required}/{country:required}")]
        [ProducesResponseType(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, Type = typeof(CityInformation))]
        [ProducesResponseType(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CityInformation>> GetCityInformation(string cityName, string country)
        {
            try
            {
                var resultTask = _cityInfoFetcher.GetAllCityInformaion(cityName, country);
                var fetchedCityInformation = await resultTask;
                return Ok(JsonConvert.SerializeObject(fetchedCityInformation, _jsonSerializerSettings));
            }
            catch (Exception e)
            {
                _logger.LogWarning("There was an error fetching the daily weatherforecasts ! \n Reason: " + e.Message + "\n Stacktrace: " + e.StackTrace);
                return NotFound("Something went wrong !");
            }
        }
    }
}