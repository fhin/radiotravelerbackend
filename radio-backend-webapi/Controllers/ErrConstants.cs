﻿namespace radio_backend.Controllers
{
    public class ErrConstants
    {
        public static readonly string ERR_IDX_GENERATION = "Error while generating random indizes !";
        public static readonly string ERR_IDX_MAX_RETRIES_REACHED = "Could not generate unique random index after maximum number of retries !";
        public static readonly string ERR_LOADING_STATIONS = "Error loading radio stations !";
    }
}
