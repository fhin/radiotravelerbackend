﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using radio_backend.Models.Radio;
using radio_backend_models.GeneralConstants;
using radio_backend_models.Radio;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace radio_backend.Controllers
{
    [Route("api/radioStation")]
    [ApiController]
    public class RadioStationController : ControllerBase
    {
        private readonly ILogger<RadioStationController> _logger;
        private readonly IRadioStationFetcher _radioStationFetcher;
        private readonly JsonSerializerSettings _jsonSerializerSettings;

        public RadioStationController(ILogger<RadioStationController> logger, IRadioStationFetcher radioStationFetcher)
        {
            _logger = logger;
            _radioStationFetcher = radioStationFetcher; 
            _jsonSerializerSettings = new JsonSerializerSettings()
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                NullValueHandling = NullValueHandling.Include,
                Formatting = Formatting.Indented
            };
        }

        [HttpGet("fetch")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RadioStation[]))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<RadioStation>>> GetRandomRadioStations()
        {
            if (_radioStationFetcher == null) return NotFound();
            try
            {
                return Ok(JsonConvert.SerializeObject((await _radioStationFetcher.FetchRandomStations())
                    .Take(GeneralConstants.MAX_STATIONS_PER_FETCH), _jsonSerializerSettings));
            }
            catch (System.Exception)
            {
                return NotFound();
            }
        }
    }
}
