﻿namespace radio_backend_models.Radio
{
    public class City
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CountryName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public City(string id, string name, string countryName, double latitude, double longitude)
        {
            Id = id;
            Name = name;
            CountryName = countryName;
            Latitude = latitude;
            Longitude = longitude;
        }
    }
}
