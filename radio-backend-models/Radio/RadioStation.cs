﻿using Newtonsoft.Json;
using System;

namespace radio_backend_models.Radio
{
    public class RadioStation
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("stationName")]
        public string Name { get; set; }
        [JsonProperty("stationWebsite")]
        public string StationWebsite { get; set; }
        [JsonProperty("latitude")]
        public double Latitude { get; set; }
        [JsonProperty("longitude")]
        public double Longitude { get; set; }
        [JsonProperty("cityName")]
        public string CityName { get; set; }
        [JsonProperty("country")]
        public string CountryName { get; set; }
        [JsonProperty("audioSourceURI")]
        public string AudioStreamURI { get; set; }

        public override string ToString()
        {
            try
            {
                string countryName = (CountryName == null || string.IsNullOrWhiteSpace(CountryName)) ? "<COUNTRY_NAME_ERR>" : CountryName;
                string id = (Id == null || string.IsNullOrWhiteSpace(Id)) ? "<ID_ERR>" : Id;
                string stationName = (Name == null || string.IsNullOrWhiteSpace(Name)) ? "<STATION_NAME_ERR>" : Name;
                string audioStream = (AudioStreamURI == null || string.IsNullOrWhiteSpace(AudioStreamURI)) ? "<AUDIO_STREAM_ERR>" : AudioStreamURI;
                string stringFormat = "ID: {0:s} | StationName: {1:s} | Country: {2:s} \n" +
                    "Latitude: {3:f} | Longitude: {4:f} | AudioStreamURI: {5:s}";
                return string.Format(stringFormat, new object[6]
                {
                    id, stationName, countryName, Latitude, Longitude, audioStream
                });
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message);
            }

        }
    }
}
