﻿using System.Net.Http;
using System.Threading.Tasks;

namespace radio_backend.Models.Request
{
    public class RequestHelper : IRequestHelper
    {
        private readonly HttpClient _httpClient;

        public RequestHelper(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public Task<HttpResponseMessage> SendRequest(Request request, bool responseAsJson)
        {
            try
            {
                string requestString = request.Encode();
                HttpRequestMessage requestMsg = new HttpRequestMessage(request.HttpMethod, requestString);
                requestMsg.Headers.Add("Accept", responseAsJson ? "application/json" : "application/text");
                return _httpClient.SendAsync(requestMsg);
            }
            catch (System.InvalidOperationException)
            {
                HttpResponseMessage excpMsg = new HttpResponseMessage(System.Net.HttpStatusCode.NoContent)
                {
                    Content = new StringContent(string.Empty)
                };
                return Task.FromResult(excpMsg);
            }
            catch (HttpRequestException)
            {
                HttpResponseMessage excpMsg = new HttpResponseMessage(System.Net.HttpStatusCode.NoContent)
                {
                    Content = new StringContent(string.Empty)
                };
                return Task.FromResult(excpMsg);
            }
        }
    }
}
