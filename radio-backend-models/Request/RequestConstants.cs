﻿namespace radio_backend.Models.Request
{
    public class RequestConstants
    {
        // See: https://www.reddit.com/r/RadioGarden/comments/igbrqd/radio_garden_api/
        // Radio garden api constants
        // City information
        public static readonly string CITY_INFO_WRAPPER_KEY = "data";
        public static readonly string CITY_INFO_DATA_WRAPPER_KEY = "list";
        public static readonly string CITY_ID_KEY = "id";
        public static readonly string CITY_NAME_KEY = "title";
        public static readonly string CITY_COUNTRY_KEY = "country";
        public static readonly string CITY_GEOLOCATION_KEY = "geo";

        // Stations for city
        public static readonly string CITY_STATIONS_INFORMATION_PREFIX = "http://radio.garden/api/ara/content/page/{0:s}";
        public static readonly string CITY_RADIO_WRAPPER_KEY = "data";
        public static readonly string CITY_RADIO_STATION_COUNT_KEY = "count";
        public static readonly string CITY_RADIO_DATA_WRAPPER_KEY = "content";
        public static readonly string CITY_RADIO_DATA_TARGET_KEY = "Stations in {0:s}";
        public static readonly string CITY_RADIO_DATA_KEY = "items";
        public static readonly string CITY_RADIO_LINK_KEY = "href";

        // Station information
        public static readonly string STATION_INFORMATION_PREFIX = "http://radio.garden/api/ara/content/channel/{0:s}";
        public static readonly string STATION_WRAPPER_KEY = "data";
        public static readonly string STATION_ID_KEY = "id";
        public static readonly string STATION_NAME_KEY = "title";
        public static readonly string STATION_WEBSITE_KEY = "website";

        // Radio garden listen constants
        public static readonly string RADIO_GARDEN_LISTEN_URI = "https://radio.garden/api/ara/content/listen/{0:s}/channel.mp3";

        // Weather api constants
        public static readonly string WEATHER_API_PREFIX = "https://api.openweathermap.org/data/2.5/onecall";
        public static readonly string WEATHER_API_KEY = "40dbf4b74e07665db545e554c5cff5dc";
        public static readonly string WREQ_LATITUDE_KEY = "lat";
        public static readonly string WREQ_LONGITUDE_KEY = "lon";
        public static readonly string WREQ_LANGUAGE_KEY = "lang";
        public static readonly string WREQ_EXCLUDE_KEY = "exclude";
        public static readonly string WREQ_UNIT_KEY = "units";
        public static readonly string WREQ_APPID_KEY = "appid";

        public static readonly string METRIC_UNIT_KEY = "metric";
        public static readonly string IMPERIAL_UNIT_KEY = "imperial";
        public static readonly string STANDARD_UNIT_KEY = "standard";

        public static readonly string WREQ_DEFAULT_LANG = "en";
        public static readonly string[] WREQ_DAILY_EXCLUDE_FIELDS = new string[4] { "current", "minutely", "hourly", "alerts"};
        public static readonly string[] WREQ_HOURLY_EXCLUDE_FIELDS = new string[4] { "current", "minutely", "daily", "alerts" };

        // Weather API json response keys
        public static readonly string DAILY_JSON_KEY = "daily";
        public static readonly string HOURLY_JSON_KEY = "hourly";

        public static readonly string TEMP_JSON_KEY = "temp";
        public static readonly string TEMP_MIN_JSON_KEY = "min";
        public static readonly string TEMP_MAX_JSON_KEY = "max";
        public static readonly string HUMID_JSON_KEY = "humidity";
        public static readonly string RAIN_JSON_KEY = "rain";
        public static readonly string WEATHER_JSON_KEY = "weather";
        public static readonly string WEATHER_MAIN_JSON_KEY = "main";
        public static readonly string WEATHER_DESCR_JSON_KEY = "description";
        public static readonly string WEATHER_ICON_JSON_KEY = "icon";
        public static readonly string HOURLY_RAIN_KEY = "1h";

        // Weather API icon constants
        public static readonly string WEATHER_ICON_PREFIX = "https://openweathermap.org/img/wn/{0:s}@{1:s}.png";
        public static readonly string WEATHER_ICON_SMALL = "2x";
        public static readonly string WEATHER_ICON_MEDIUM = "4x";

        // TODO: Localization support
        // (Wiki|Travel)-pedia API constants
        public static readonly string WIKI_QUERY_PAGE_TITLE_PREFIX = "titles";
        public static readonly string WIKI_PARSE_PAGE_TITLE_PREFIX = "page";
        public static readonly string WIKI_PAGEID_KEY = "pageid";
        public static readonly string WIKI_CITY_COUNTRY_SEPERATOR = ",_";
        public static readonly string WIKI_SECTION_NUMBER_PREFIX = "section";
        public static readonly string WIKI_RESPONSE_DATA_FORMAT_PREFIX = "format";

        public static readonly string GENERAL_WIKI_FLAGS = "redirects=1";
        public static readonly string GENERAL_WIKI_CONTENT_FLAGS = "disabletoc=1&disableeditsection&disablestyleduplication&disablelimitreport";

        // MediaWiki API documentation for action=query: https://www.mediawiki.org/w/api.php?action=help&modules=query
        public static readonly string WIKI_API_LOCATION_SUMMARY_URI = "https://en.wikipedia.org/w/api.php?action=query" +
            "&prop=extracts&exintro&explaintext" + "&" + GENERAL_WIKI_FLAGS + "&" + GENERAL_WIKI_CONTENT_FLAGS;

        // MediaWiki API documentation for action=parse: https://www.mediawiki.org/w/api.php?action=help&modules=parse
        public static readonly string WIKI_API_ALL_SECTIONS_URI = "https://en.wikipedia.org/w/api.php?action=parse" +
            "&prop=sections"+  "&" + GENERAL_WIKI_FLAGS;
        public static readonly string WIKI_API_ONE_SECTION_CONTENT_URI = "https://en.wikipedia.org/w/api.php?action" +
            "&prop=text&explaintext" + "&" + GENERAL_WIKI_FLAGS + "&" + GENERAL_WIKI_CONTENT_FLAGS;

        public static readonly string TRAVEL_WIKI_API_ALL_SECTIONS_URI = "https://en.wikivoyage.org/w/api.php?action=parse" +
            "&prop=sections&disabletoc=1" + "&" + GENERAL_WIKI_FLAGS;
        public static readonly string TRAVEL_WIKI_API_ONE_SECTION_CONTENT_URI = "https://en.wikivoyage.org/w/api.php?action=parse" +
            "&prop=text" + "&" + GENERAL_WIKI_FLAGS + "&" + GENERAL_WIKI_CONTENT_FLAGS;

        // MediaWiki API response json fields
        public static readonly string WIKI_API_PARSE_HEAD_KEY = "parse";
        public static readonly string WIKI_API_QUERY_HEAD_KEY = "query";
        public static readonly string WIKI_API_QUERY_CONTENT_KEY = "pages";
        public static readonly string WIKI_API_QUERY_SUMMARY_KEY = "extract";
        public static readonly string WIKI_API_PARSE_SECTION_CONTENT_KEY = "text";
        public static readonly string WIKI_API_JSON_SECTION_NAME_KEY = "line";
        public static readonly string WIKI_API_JSON_SECTION_INDEX_KEY = "index";
        public static readonly string WIKI_API_JSON_SECTION_NUMBER_KEY = "number";
        public static readonly string WIKI_API_JSON_SECTIONS_KEY = "sections";
    }
}
