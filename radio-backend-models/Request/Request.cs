﻿using radio_backend_models.GeneralConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace radio_backend.Models.Request
{
    public enum RequestURLType
    {
        STATIC, DYNAMIC
    }

    public class Request
    {
        public HttpMethod HttpMethod { get; private set; }

        private bool _appendParameters;
        private int _baseUrlParameterIdx;
        private int _baseUrlParameterCnt;
        private RequestURLType _requestUrlType;
        private string _baseUrl;
        private IList<string> _baseUrlParameters;
        private IDictionary<string, Type> _formatParameterDict;
        private IDictionary<string, object> _actualParameterDict;
        private IEnumerable<string> _parameterOrder;
        private bool _parameterOrderFixed;

        public Request(RequestURLType requestURLType, HttpMethod method, string baseUrl)
        {
            _requestUrlType = requestURLType;
            if (string.IsNullOrWhiteSpace(baseUrl)) throw new ArgumentException("");
            HttpMethod = method;
            _baseUrl = baseUrl;
            _formatParameterDict = new Dictionary<string, Type>();
            _actualParameterDict = new Dictionary<string, object>();
            _parameterOrder = new string[0];
            _parameterOrderFixed = false;
        }

        public Request(RequestURLType requestURLType, HttpMethod method, string baseUrl, bool appendParameters)
            : this(requestURLType, method, baseUrl)
        {
            _appendParameters = appendParameters;
        }

        public Request(RequestURLType requestURLType, HttpMethod method, string baseUrl, int baseUrlParamCnt)
            : this(requestURLType, method, baseUrl)
        {
            // TODO
            if (baseUrlParamCnt <= 0) throw new ArgumentException("");
            _baseUrlParameters = new string[baseUrlParamCnt];
            _baseUrlParameterCnt = baseUrlParamCnt;
            _baseUrlParameterIdx = 0;
        }

        public void AddBaseUrlParameter(string urlParameter)
        {
            // TODO: 
            if (_requestUrlType == RequestURLType.STATIC) throw new InvalidOperationException("");
            if (string.IsNullOrWhiteSpace(urlParameter)) throw new ArgumentException("");
            if (_baseUrlParameterIdx >= _baseUrlParameterCnt) throw new ArgumentException("");
            _baseUrlParameters[_baseUrlParameterIdx++] = urlParameter;

        }

        public void AddFormalParameter(string parameterName, Type paramType)
        {
            if (_parameterOrderFixed) throw new ArgumentException("");
            if (string.IsNullOrWhiteSpace(parameterName)) throw new ArgumentException("");
            if (_formatParameterDict.ContainsKey(parameterName)) throw new ArgumentException("");

            _formatParameterDict.Add(parameterName, paramType);
            return;
        }

        public void FixParameterOrder(IEnumerable<string> parameterNameArr)
        {
            if (_parameterOrderFixed) return;
            if (parameterNameArr == null) return;
            string[] orderedParamArr = new string[_formatParameterDict.Keys.Count];
            int paramCnter = 0;
            foreach (var parameterName in parameterNameArr)
            {
                if (string.IsNullOrWhiteSpace(parameterName)) throw new ArgumentException("");
                if (!_formatParameterDict.ContainsKey(parameterName)) throw new ArgumentException("");
                orderedParamArr[paramCnter++] = parameterName;
            }
            if (paramCnter != _formatParameterDict.Keys.Count) throw new ArgumentException("");
            _parameterOrder = orderedParamArr;
            _parameterOrderFixed = true;
            return;
        }

        public void AddActualParameter(string parameterName, object parameterValue)
        {
            if (string.IsNullOrWhiteSpace(parameterName) || parameterValue == null) throw new ArgumentException("");
            if (!_formatParameterDict.ContainsKey(parameterName)) return;
            if (_actualParameterDict.ContainsKey(parameterName) && _actualParameterDict[parameterName] != null) return;

            Type formalParamType = _formatParameterDict[parameterName];
            Type actualType = parameterValue.GetType();
            if (!formalParamType.IsAssignableFrom(actualType)) throw new ArgumentException("");

            _actualParameterDict.Add(parameterName, parameterValue);
            return;
        }
    
        public string Encode()
        {
            StringBuilder sb;
            if (_requestUrlType == RequestURLType.STATIC)
            {
                sb = new StringBuilder(_baseUrl);
            }
            else
            {
                var formattedUrlParameters = _baseUrlParameters.Select(baseUrlParam => HttpUtility.UrlEncode(baseUrlParam)).ToArray();
                sb = new StringBuilder(string.Format(_baseUrl, formattedUrlParameters));
            }
            int paramCnter = 0;
            foreach (var param in _parameterOrder)
            {
                if (string.IsNullOrWhiteSpace(param)) throw new ArgumentException("");
                if (!_formatParameterDict.ContainsKey(param) || !_actualParameterDict.ContainsKey(param)) throw new ArgumentException("");

                if (_appendParameters)
                {
                    sb.Append(GeneralConstants.HTTP_PARAM_PREFIX);
                }
                else
                {
                    sb.Append(paramCnter == 0 ? GeneralConstants.FIRST_HTTP_PARAM_PREFIX : GeneralConstants.HTTP_PARAM_PREFIX);
                }
                sb.Append(HttpUtility.UrlEncode(param) + "=");
                Type paramType = _formatParameterDict[param];
                if (paramType.IsArray)
                {
                    System.Collections.IEnumerable paramValArr = _actualParameterDict[param] as System.Collections.IEnumerable;
                    bool emptyArr = true;
                    foreach (var arrVal in paramValArr)
                    {
                        if (arrVal == null) throw new ArgumentException("");
                        sb.Append(HttpUtility.UrlEncode(arrVal.ToString()) + GeneralConstants.HTTP_LIST_PARAM_VALUE_PARAM);
                        emptyArr = false;
                    }
                    if (!emptyArr)
                    {
                        sb.Remove(sb.Length - 1, 1);
                    }
                }
                else
                {
                    sb.Append(HttpUtility.UrlEncode(_actualParameterDict[param].ToString()));
                }
                paramCnter++;
            }
            return sb.ToString();
        }
    }
}
