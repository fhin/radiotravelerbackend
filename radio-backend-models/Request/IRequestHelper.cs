﻿using System.Net.Http;
using System.Threading.Tasks;

namespace radio_backend.Models.Request
{
    public interface IRequestHelper
    {
        public Task<HttpResponseMessage> SendRequest(Request request, bool responseAsJson);
    }
}
