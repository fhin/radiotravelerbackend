﻿using System;
using System.Net.Http;

namespace radio_backend.Models.Request
{
    public enum RequestType
    {
        HOURLY_WEATHER_FORECAST,
        DAILY_WEATHER_FORECAST,
        WEATHER_ICON,
        WIKI_ALL_SECTIONS,
        WIKI_ONE_SECTION_CONTENT,
        TRAVEL_WIKI_ALL_SECTIONS,
        TRAVEL_WIKI_ONE_SECTION_CONTENT,
        WIKI_SUMMARY,

        STATIONS_FOR_CITY,
        STATION_INFO
    }

    public class RequestFactory
    {
        public RequestFactory()
        {
        }

        public Request GetRequest(RequestType rType)
        {
            switch (rType)
            {
                case RequestType.HOURLY_WEATHER_FORECAST:
                    Request hWeatherRequest = new Request(RequestURLType.STATIC, HttpMethod.Get, RequestConstants.WEATHER_API_PREFIX);
                    hWeatherRequest.AddFormalParameter(RequestConstants.WREQ_LATITUDE_KEY, typeof(double));
                    hWeatherRequest.AddFormalParameter(RequestConstants.WREQ_LONGITUDE_KEY, typeof(double));
                    hWeatherRequest.AddFormalParameter(RequestConstants.WREQ_LANGUAGE_KEY, typeof(string));
                    hWeatherRequest.AddFormalParameter(RequestConstants.WREQ_EXCLUDE_KEY, typeof(string[]));
                    hWeatherRequest.AddFormalParameter(RequestConstants.WREQ_UNIT_KEY, typeof(string));
                    hWeatherRequest.AddFormalParameter(RequestConstants.WREQ_APPID_KEY, typeof(string));
                    hWeatherRequest.FixParameterOrder(new string[6]{
                        RequestConstants.WREQ_LATITUDE_KEY, RequestConstants.WREQ_LONGITUDE_KEY,
                        RequestConstants.WREQ_LANGUAGE_KEY, RequestConstants.WREQ_EXCLUDE_KEY,
                        RequestConstants.WREQ_UNIT_KEY, RequestConstants.WREQ_APPID_KEY
                    });
                    hWeatherRequest.AddActualParameter(RequestConstants.WREQ_EXCLUDE_KEY, RequestConstants.WREQ_HOURLY_EXCLUDE_FIELDS);
                    hWeatherRequest.AddActualParameter(RequestConstants.WREQ_LANGUAGE_KEY, RequestConstants.WREQ_DEFAULT_LANG);
                    hWeatherRequest.AddActualParameter(RequestConstants.WREQ_APPID_KEY, RequestConstants.WEATHER_API_KEY);
                    return hWeatherRequest;
                case RequestType.DAILY_WEATHER_FORECAST:
                    Request dWeatherRequest = new Request(RequestURLType.STATIC, HttpMethod.Get, RequestConstants.WEATHER_API_PREFIX);
                    dWeatherRequest.AddFormalParameter(RequestConstants.WREQ_LATITUDE_KEY, typeof(double));
                    dWeatherRequest.AddFormalParameter(RequestConstants.WREQ_LONGITUDE_KEY, typeof(double));
                    dWeatherRequest.AddFormalParameter(RequestConstants.WREQ_LANGUAGE_KEY, typeof(string));
                    dWeatherRequest.AddFormalParameter(RequestConstants.WREQ_EXCLUDE_KEY, typeof(string[]));
                    dWeatherRequest.AddFormalParameter(RequestConstants.WREQ_UNIT_KEY, typeof(string));
                    dWeatherRequest.AddFormalParameter(RequestConstants.WREQ_APPID_KEY, typeof(string));
                    dWeatherRequest.FixParameterOrder(new string[6]{
                        RequestConstants.WREQ_LATITUDE_KEY, RequestConstants.WREQ_LONGITUDE_KEY,
                        RequestConstants.WREQ_LANGUAGE_KEY, RequestConstants.WREQ_EXCLUDE_KEY,
                        RequestConstants.WREQ_UNIT_KEY, RequestConstants.WREQ_APPID_KEY
                    });
                    dWeatherRequest.AddActualParameter(RequestConstants.WREQ_EXCLUDE_KEY, RequestConstants.WREQ_DAILY_EXCLUDE_FIELDS);
                    dWeatherRequest.AddActualParameter(RequestConstants.WREQ_LANGUAGE_KEY, RequestConstants.WREQ_DEFAULT_LANG);
                    dWeatherRequest.AddActualParameter(RequestConstants.WREQ_APPID_KEY, RequestConstants.WEATHER_API_KEY);
                    return dWeatherRequest;
                case RequestType.WEATHER_ICON:
                    return new Request(RequestURLType.DYNAMIC, HttpMethod.Get, RequestConstants.WEATHER_ICON_PREFIX, 2);
                case RequestType.WIKI_SUMMARY:
                    Request summaryRequest = new Request(RequestURLType.STATIC, HttpMethod.Get, RequestConstants.WIKI_API_LOCATION_SUMMARY_URI, true);
                    summaryRequest.AddFormalParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, typeof(string));
                    summaryRequest.AddFormalParameter(RequestConstants.WIKI_QUERY_PAGE_TITLE_PREFIX, typeof(string));
                    summaryRequest.FixParameterOrder(new string[2]
                    {
                        RequestConstants.WIKI_QUERY_PAGE_TITLE_PREFIX, RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX
                    });
                    summaryRequest.AddActualParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, "json");
                    return summaryRequest;
                case RequestType.WIKI_ALL_SECTIONS:
                    Request stdAllSectionsRequest = new Request(RequestURLType.STATIC, HttpMethod.Get, RequestConstants.WIKI_API_ALL_SECTIONS_URI, true);
                    stdAllSectionsRequest.AddFormalParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, typeof(string));
                    stdAllSectionsRequest.AddFormalParameter(RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, typeof(string)); 
                    stdAllSectionsRequest.FixParameterOrder(new string[2]
                     {
                        RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX
                     });
                    stdAllSectionsRequest.AddActualParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, "json");
                    return stdAllSectionsRequest;
                case RequestType.WIKI_ONE_SECTION_CONTENT:
                    Request stdOneSectionRequest = new Request(RequestURLType.STATIC, HttpMethod.Get, RequestConstants.WIKI_API_ONE_SECTION_CONTENT_URI, true);
                    stdOneSectionRequest.AddFormalParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, typeof(string));
                    stdOneSectionRequest.AddFormalParameter(RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, typeof(string));
                    stdOneSectionRequest.AddFormalParameter(RequestConstants.WIKI_SECTION_NUMBER_PREFIX, typeof(uint));
                    stdOneSectionRequest.FixParameterOrder(new string[3]
                    {
                        RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, RequestConstants.WIKI_SECTION_NUMBER_PREFIX,
                        RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX
                    });
                    stdOneSectionRequest.AddActualParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, "json");
                    return stdOneSectionRequest;
                case RequestType.TRAVEL_WIKI_ALL_SECTIONS:
                    Request travelAllSectionsRequest = new Request(RequestURLType.STATIC, HttpMethod.Get, 
                        RequestConstants.TRAVEL_WIKI_API_ALL_SECTIONS_URI, true);
                    travelAllSectionsRequest.AddFormalParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, typeof(string));
                    travelAllSectionsRequest.AddFormalParameter(RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, typeof(string));
                    travelAllSectionsRequest.FixParameterOrder(new string[2]
                     {
                        RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX
                     });
                    travelAllSectionsRequest.AddActualParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, "json");
                    return travelAllSectionsRequest;
                case RequestType.TRAVEL_WIKI_ONE_SECTION_CONTENT:
                    Request travelOneSectionRequest = new Request(RequestURLType.STATIC, HttpMethod.Get, 
                        RequestConstants.TRAVEL_WIKI_API_ONE_SECTION_CONTENT_URI, true);
                    travelOneSectionRequest.AddFormalParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, typeof(string));
                    travelOneSectionRequest.AddFormalParameter(RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, typeof(string));
                    travelOneSectionRequest.AddFormalParameter(RequestConstants.WIKI_SECTION_NUMBER_PREFIX, typeof(uint));
                    travelOneSectionRequest.FixParameterOrder(new string[3]
                    {
                        RequestConstants.WIKI_PARSE_PAGE_TITLE_PREFIX, RequestConstants.WIKI_SECTION_NUMBER_PREFIX,
                        RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX
                    });
                    travelOneSectionRequest.AddActualParameter(RequestConstants.WIKI_RESPONSE_DATA_FORMAT_PREFIX, "json");
                    return travelOneSectionRequest;
                case RequestType.STATIONS_FOR_CITY:
                    return new Request(RequestURLType.DYNAMIC, HttpMethod.Get,
                        RequestConstants.CITY_STATIONS_INFORMATION_PREFIX, 1);
                case RequestType.STATION_INFO:
                    return new Request(RequestURLType.DYNAMIC, HttpMethod.Get,
                        RequestConstants.STATION_INFORMATION_PREFIX, 1);
                default:
                    throw new ArgumentException("");
            }
        }
    }
}
