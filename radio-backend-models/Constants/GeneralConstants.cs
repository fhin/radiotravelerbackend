﻿namespace radio_backend_models.GeneralConstants
{
    public class GeneralConstants
    {
        public static readonly int MAX_STATIONS_PER_FETCH = 5;
        public static readonly int MAX_IMPRESSIONS_PER_FETCH = 5;
        public static readonly int MAX_WEATHER_FORECAST_PER_FETCH = 7;

        public static readonly int MAX_REQUEST_TRIES = 5;
        public static readonly int MAX_IDX_RETRIES = 100;
        // Request limit is given by: https://openweathermap.org/price
        public static readonly int MAX_REQUEST_LIMIT = 1000;
        public static readonly string STREAM_PREFIX = "https://radio.garden";
        public static readonly string FIRST_HTTP_PARAM_PREFIX = "?";
        public static readonly string HTTP_PARAM_PREFIX = "&";
        public static readonly string HTTP_LIST_PARAM_VALUE_PARAM = ",";


        // Radio station json property names
        public static readonly string RADIO_ID_KEY = "id";
        public static readonly string RADIO_STATIONNAME_KEY = "stationName";
        public static readonly string RADIO_WEBSITE_KEY = "stationWebsite";
        public static readonly string RADIO_STATION_LATITUDE_KEY = "latitude";
        public static readonly string RADIO_STATION_LONGITUDE_KEY = "longitude";
        public static readonly string RADIO_CITYNAME_KEY = "cityName";
        public static readonly string RADIO_COUNTRYNAME_KEY = "country";
        public static readonly string RADIO_AUDIOSTREAM_KEY = "audioSourceURI";
    }
}
