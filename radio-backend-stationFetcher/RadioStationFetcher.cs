﻿using Newtonsoft.Json.Linq;
using radio_backend.Models.Request;
using radio_backend_models.GeneralConstants;
using radio_backend_models.Radio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace radio_backend_stationFetcher
{
    public class RadioStationFetcher
    {
        private readonly int REQUEST_DELAY_IN_MS = 3000;
        private readonly int CITY_SUBSET_SIZE = 500;
        private readonly string COUNTRY_RESOURCE_LOCATION;
        private readonly RequestFactory _reqFactory;
        private readonly RequestHelper _reqHelper;
        private readonly bool _useSubsetOfCities;
        public RadioStationFetcher(string cityResourceFilePath, bool useSubsetOfCities)
        {
            if (string.IsNullOrWhiteSpace(cityResourceFilePath) || !File.Exists(cityResourceFilePath))
            {
                throw new ArgumentException("");
            }
            COUNTRY_RESOURCE_LOCATION = cityResourceFilePath;
            _reqFactory = new RequestFactory();
            _reqHelper = new RequestHelper(new System.Net.Http.HttpClient());
            _useSubsetOfCities = useSubsetOfCities;
        }

        public async Task<IEnumerable<RadioStation>> GetAllStations()
        {
            List<RadioStation> radioStations = new List<RadioStation>();
            var availableCities = await GetAvailableCities();
            var stationsPerCityTasks = Interleaved<IEnumerable<RadioStation>>(availableCities.Select(async (currCity) =>
            {
                try
                {
                    await Task.Delay(REQUEST_DELAY_IN_MS);
                    var stationsForCity = await GetStationsForCity(currCity)
                        .ContinueWith(finishedTask =>
                        {
                            try
                            {
                                if (finishedTask.IsFaulted)
                                {
                                    throw new ArgumentException("Error");
                                }
                                else if (finishedTask.IsCanceled)
                                {
                                    throw new ArgumentException("Canceled");
                                }
                                else
                                {
                                    return finishedTask.Result.Where(fetchedStation => fetchedStation != null)
                                    .Take(GeneralConstants.MAX_STATIONS_PER_FETCH);
                                }
                            }
                            catch (ArgumentException e)
                            {
                                return new RadioStation[0];
                            }
                        });
                    return stationsForCity;
                }
                catch (Exception e)
                {
                    // TODO: Logging
                    return new RadioStation[0];
                }
            }));

            foreach (var bucket in stationsPerCityTasks)
            {
                var bucketT = await bucket;
                IEnumerable<RadioStation> stationsForCity = await bucketT;
                if (stationsForCity != null && stationsForCity.Any())
                {
                    radioStations.AddRange(stationsForCity);
                }
            }
            return radioStations;
        }

        private static Task<Task<T>>[] Interleaved<T>(IEnumerable<Task<T>> tasks)
        {
            var inputTasks = tasks.ToList();

            var buckets = new TaskCompletionSource<Task<T>>[inputTasks.Count];
            var results = new Task<Task<T>>[buckets.Length];
            for (int i = 0; i < buckets.Length; i++)
            {
                buckets[i] = new TaskCompletionSource<Task<T>>();
                results[i] = buckets[i].Task;
            }

            int nextTaskIndex = -1;
            Action<Task<T>> continuation = completed =>
            {
                var bucket = buckets[Interlocked.Increment(ref nextTaskIndex)];
                bucket.TrySetResult(completed);
            };

            foreach (var inputTask in inputTasks)
                inputTask.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);

            return results;
        }

        private Task<IEnumerable<City>> GetAvailableCities()
        {
            return Task.Run<IEnumerable<City>>(() =>
            {
                try
                {
                    string fileContent = File.ReadAllText(COUNTRY_RESOURCE_LOCATION);
                    JObject cityInformationJObj = JObject.Parse(fileContent);
                    IList<JToken> countryEntries = cityInformationJObj[RequestConstants.CITY_INFO_WRAPPER_KEY]
                        [RequestConstants.CITY_INFO_DATA_WRAPPER_KEY].Children().ToList();

                    var availableCities = countryEntries.Select(countryEntry =>
                    {
                        City parsedCity;
                        try
                        {
                            string cityName = countryEntry.Value<string>(RequestConstants.CITY_NAME_KEY);
                            string countryName = countryEntry.Value<string>(RequestConstants.CITY_COUNTRY_KEY);
                            string id = countryEntry.Value<string>(RequestConstants.CITY_ID_KEY);

                            JArray geoLocationArray = countryEntry[RequestConstants.CITY_GEOLOCATION_KEY] as JArray;
                            var geoLocationData = geoLocationArray.Values<double>().ToArray();

                            double latitude = geoLocationData[1];
                            double longitude = geoLocationData[0];
                            return new City(id, cityName, countryName, latitude, longitude);
                        }
                        catch (System.Exception)
                        {
                            // TODO: Logging
                            parsedCity = null;
                        }
                        return parsedCity;
                    }).Where(city => city != null).ToList();
                    if (_useSubsetOfCities)
                    {
                        double N = availableCities.Count;
                        int M = CITY_SUBSET_SIZE;
                        double numItemsLeft = M;
                        int currItemIdx = 0;
                        double currProb = numItemsLeft / N;
                        Random randomGenerator = new Random();
                        City[] subsetOfCities = new City[M];

                        foreach (var city in availableCities)
                        {
                            if (randomGenerator.NextDouble() < currProb)
                            {
                                numItemsLeft--;
                                subsetOfCities[currItemIdx++] = city;
                            }
                            N--;
                            currProb = numItemsLeft / N;
                            if (numItemsLeft == 0) break;
                        }

                        return subsetOfCities;
                    }
                    return availableCities;
                }
                catch (System.Exception e)
                {
                    throw new ArgumentException("");
                }
            });
        }

        private async Task<IEnumerable<RadioStation>> GetStationsForCity(City city)
        {
            if (city == null || string.IsNullOrWhiteSpace(city.Id) || string.IsNullOrWhiteSpace(city.Name) ||
                string.IsNullOrWhiteSpace(city.CountryName)) throw new ArgumentException("");

            try
            {
                Request stationsForCityRequest = _reqFactory.GetRequest(RequestType.STATIONS_FOR_CITY);
                stationsForCityRequest.AddBaseUrlParameter(city.Id);
                var response = await _reqHelper.SendRequest(stationsForCityRequest, true);
                if (!response.IsSuccessStatusCode)
                {
                    throw new ArgumentException("");
                }
                string responseText = await response.Content.ReadAsStringAsync();
                JObject cityStationsObject = JObject.Parse(responseText);

                int stationCount = cityStationsObject[RequestConstants.CITY_RADIO_WRAPPER_KEY]
                    [RequestConstants.CITY_RADIO_STATION_COUNT_KEY].Value<int>();
                if (stationCount < 0)
                {
                    throw new ArgumentException("");
                }

                // Parse list of short station descriptions
                RadioStation[] radioStations = new RadioStation[stationCount];
                JToken stationsForCityWrapperItem = cityStationsObject[RequestConstants.CITY_RADIO_WRAPPER_KEY]
                    [RequestConstants.CITY_RADIO_DATA_WRAPPER_KEY].Children().First();

                IList<JToken> shortStationEntries = stationsForCityWrapperItem[RequestConstants.CITY_RADIO_DATA_KEY]
                    .Children().ToList();

                return shortStationEntries.Select((shortStationEntry, idx) =>
                {
                    try
                    {
                        string stationTitle = shortStationEntry[RequestConstants.STATION_NAME_KEY].Value<string>();
                        string stationLink = shortStationEntry[RequestConstants.CITY_RADIO_LINK_KEY].Value<string>();
                        int lastSlashIdx = stationLink.LastIndexOf('/');
                        string stationId = stationLink.Substring(lastSlashIdx+1);

                        return new RadioStation()
                        {
                            Id = stationId,
                            Name = stationTitle,
                            Longitude = city.Longitude,
                            Latitude = city.Latitude,
                            CityName = city.Name,
                            CountryName = city.CountryName,
                            AudioStreamURI = string.Format(RequestConstants.RADIO_GARDEN_LISTEN_URI, stationId)
                        };
                    }
                    catch (Exception e)
                    {
                        // TODO: Exception handling
                        return null;
                    }
                }).Where(fetchedStation => fetchedStation != null);
            }
            catch (Exception e)
            {
                throw new ArgumentException("");
            }
        }
    }
}
