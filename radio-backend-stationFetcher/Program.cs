﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace radio_backend_stationFetcher
{
    public class Program
    {
        public static async System.Threading.Tasks.Task Main(string[] args)
        {
            string dumpLocation = args[0];
            if (string.IsNullOrWhiteSpace(dumpLocation)) return;
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                stopwatch.Start();
                RadioStationFetcher stationFetcher = new RadioStationFetcher(@"cityInformation.txt", true);
                var stations = await stationFetcher.GetAllStations();
                stopwatch.Stop();

                using (StreamWriter sw = new StreamWriter(dumpLocation, false, System.Text.Encoding.UTF8))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    Task jsonDumpTask = Task.Run(() =>
                    {
                        sw.Write(JsonConvert.SerializeObject(stations, Formatting.Indented));
                    });
                    foreach (var station in stations)
                    {
                        try
                        {
                            Console.WriteLine(station.ToString());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error writing city ! \n" + e.Message);
                        }
                    }
                    await jsonDumpTask;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while fetching stations: " + e);
            }
            finally
            {
                stopwatch.Stop();
            }
            Console.WriteLine("Elapsed time [in ms]: " + stopwatch.ElapsedMilliseconds);
        }
    }
}
